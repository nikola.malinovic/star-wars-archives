import Navbar from './components/Navigation/Navbar/Navbar';
import PageContent from './components/PageContent/PageContent';

import './App.css';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <PageContent />
      </div>
    </BrowserRouter>
  );
}

export default App;
