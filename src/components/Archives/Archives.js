import React, { Component } from 'react';
import classes from './Archives.module.css';
import ArchivesToolbar from './ArchivesToolbar/ArchivesToolbar';
import ArchivesContent from './ArchivesContent/ArchivesContent';
import { Route, Redirect } from 'react-router';
import Details from './Details/Details';

class Archives extends Component {

    render() {
        return (
            <div className={classes.Archives}>
                <Route path="/archives/:category" exact component={ArchivesToolbar} />
                <Route path="/archives/:category" exact component={ArchivesContent} />
                <Route path="/archives" exact >
                    <Redirect to={{
                        pathname: '/archives/people',
                        search: '?page=1'
                    }} />
                </Route>
                <Route path="/archives/:category/details/:id" exact component={Details} />
            </div>
        );
    }
};

export default Archives;