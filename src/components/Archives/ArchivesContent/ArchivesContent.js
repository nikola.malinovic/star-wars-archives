import React, { Component } from 'react';
import classes from './ArchivesContent.module.css';
import axios from 'axios';
import SwaTable from '../../UI/SwaTable/SwaTable';
import Loader from '../../UI/Loader/Loader';
import Pagination from '../../UI/Pagination/Pagination';

class ArchivesContent extends Component {

    state = {
        category: 'people',
        data: [],
        loading: true,
        page: undefined,
        maxPage: undefined,
        search: null
    }

    componentDidMount() {
        this.updateContentData();
    }

    async componentDidUpdate() {
        const query = new URLSearchParams(this.props.location.search);
        const isCategoryChanged = this.state.category !== this.props.match.params['category'];
        const isPageChanged = this.state.page !== +query.get('page');
        const isSearchChanged = this.state.search !== query.get('search');
        if (isCategoryChanged || isPageChanged || isSearchChanged) {
            this.updateContentData();
        }
    }

    async updateContentData() {
        const query = new URLSearchParams(this.props.location.search);
        await this.setState({
            category: this.props.match.params['category'],
            loading: true,
            page: +query.get('page'),
            search: query.get('search')
        });
        const searchString = this.state.search == null ? '' : this.state.search;
        await axios.get(`https://swapi.dev/api/${this.props.match.params['category']}/?search=${searchString}&page=${this.state.page}`)
            .then(response => {
                let dataWithId = response.data.results.map(object => {
                    return { ...object, id: object.url.split("/")[5] };
                });
                this.setState({
                    data: dataWithId,
                    loading: false,
                    maxPage: Math.ceil(response.data.count / 10)
                });
            })
            .catch(error => {
                this.props.history.push(`/archives/${this.state.category}?page=1`);
            });
    }

    changePage(newPage) {
        if (newPage < 1)
            return;
        if (newPage > this.state.maxPage)
            return;
        const searchString = this.state.search == null ? '' : this.state.search;
        this.props.history.push(`/archives/${this.state.category}?search=${searchString}&page=${newPage}`);
    }

    render() {
        let content;

        if (this.state.loading) {
            content = <Loader />;
        } else {
            content =
                <div className={classes.ContentWrapper}>
                    <SwaTable
                        displayData={this.state.data}
                        category={this.state.category} />
                    <Pagination
                        size={this.state.maxPage}
                        initialPage={this.state.page}
                        changePage={(newPage) => this.changePage(newPage)} />
                </div>;
        }

        return (
            <div className={classes.ArchivesContent}>
                {content}
            </div>
        );
    }
};

export default ArchivesContent;