import classes from './ArchivesToolbar.module.css';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsers, faFilm, faSpaceShuttle, faMotorcycle, faUniversalAccess, faGlobe } from '@fortawesome/free-solid-svg-icons';
import SearchBar from '../../UI/SearchBar/SearchBar';

const archivesToolbar = (props) => {
    const categories = [
        { category: "people", icon: faUsers },
        { category: "films", icon: faFilm },
        { category: "starships", icon: faSpaceShuttle },
        { category: "vehicles", icon: faMotorcycle },
        { category: "species", icon: faUniversalAccess },
        { category: "planets", icon: faGlobe }
    ]

    return (
        <div className={classes.ArchivesToolbar}>
            <div className={classes.CategoryButtons}>
                {categories.map(category => (
                    <NavLink
                        to={"/archives/" + category.category + "?page=1"}
                        activeClassName={classes.activeCategory}
                        className={classes.ToolbarLink}
                        key={category.category}>
                        <div>{category.category} <FontAwesomeIcon icon={category.icon} /></div>
                    </NavLink>
                ))}
            </div>
            <SearchBar/>
        </div>
    );
};

export default archivesToolbar;