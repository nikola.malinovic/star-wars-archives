import classes from "./Details.module.css";
import { useState, useEffect } from "react";
import axios from "axios";
import Loader from "../../UI/Loader/Loader";
import DetailsHeader from "./DetailsHeader/DetailsHeader";
import DetailsContent from "./DetailsContent/DetailsContent";
import DetailsImage from "./DetailsImage/DetailsImage";
import DetailsData from "./DetailsData/DetailsData";

const Details = (props) => {

    const [data, setData] = useState(undefined);
    const [loading, setLoading] = useState(true);

    const selectedCategory = props.match.params['category'];
    const selectedId = props.match.params['id'];

    useEffect(() => {
        let source = axios.CancelToken.source();
        axios.get(`https://swapi.dev/api/${selectedCategory}/${selectedId}/`)
            .then(response => {
                setData(response.data);
                setLoading(false);
                console.log(response.data);
            })
            .catch(error => {
                this.props.history.push(`/archives/${selectedCategory}?page=1`);
            });

        return () => {
            source.cancel("Cancelling in cleanup");
        };
    }, [selectedId, selectedCategory]);

    let content;

    if (loading) {
        content = <Loader />;
    } else {
        switch (selectedCategory) {
            case 'people':
                content = (
                    <div>
                        <DetailsHeader title={data.name} />
                        <div className={classes.Row}>
                            <DetailsContent category={selectedCategory} character={data} />
                            <div className={classes.Column}>
                                <DetailsImage category={selectedCategory} id={selectedId} alt={data.name} />
                                <DetailsData category={selectedCategory} data={data} />
                            </div>
                        </div>
                    </div>
                );
                break;
            case 'films':
                content = (
                    <div>
                        <DetailsHeader title={data.title} />
                        <div className={classes.Row}>
                            <DetailsContent category={selectedCategory} film={data} />
                            <div className={classes.Column}>
                                <DetailsImage category={selectedCategory} id={selectedId} alt={data.title} />
                                <DetailsData category={selectedCategory} data={data} />
                            </div>
                        </div>
                    </div>
                );
                break;
            case 'starships':
                content = (
                    <div>
                        <DetailsHeader title={data.name} />
                        <div className={classes.Row}>
                            <DetailsContent category={selectedCategory} starship={data} />
                            <div className={classes.Column}>
                                <DetailsImage category={selectedCategory} id={selectedId} alt={data.title} />
                                <DetailsData category={selectedCategory} data={data} />
                            </div>
                        </div>
                    </div>
                );
                break;
            case 'vehicles':
                content = (
                    <div>
                        <DetailsHeader title={data.name} />
                        <div className={classes.Row}>
                            <DetailsContent category={selectedCategory} vehicle={data} />
                            <div className={classes.Column}>
                                <DetailsImage category={selectedCategory} id={selectedId} alt={data.name} />
                                <DetailsData category={selectedCategory} data={data} />
                            </div>
                        </div>
                    </div>
                );
                break;
            case 'species':
                content = (
                    <div>
                        <DetailsHeader title={data.name} />
                        <div className={classes.Row}>
                            <DetailsContent category={selectedCategory} specie={data} />
                            <div className={classes.Column}>
                                <DetailsImage category={selectedCategory} id={selectedId} alt={data.name} />
                                <DetailsData category={selectedCategory} data={data} />
                            </div>
                        </div>
                    </div>
                );
                break;
            case 'planets':
                content = (
                    <div>
                        <DetailsHeader title={data.name} />
                        <div className={classes.Row}>
                            <DetailsContent category={selectedCategory} planet={data} />
                            <div className={classes.Column}>
                                <DetailsImage category={selectedCategory} id={selectedId} alt={data.name} />
                                <DetailsData category={selectedCategory} data={data} />
                            </div>
                        </div>
                    </div>
                );
                break;
            default:
                content = <h1>Ooops, something went wrong!</h1>
                break;
        }
    }

    return content;
}

export default Details;