import classes from "./DetailsContent.module.css";
import OpeningCrawl from "../Subcategories/OpeningCrawl/OpeningCrawl";
import CharactersSubcategory from "../Subcategories/CharactersSubcategory/CharactersSubcategory";
import FilmsSubcategory from "../Subcategories/FilmsSubcategory/FilmsSubcategory";
import StarshipSubcategory from "../Subcategories/StarshipSubcategory/StarshipSubcategory";
import VehiclesSubcategory from "../Subcategories/VehiclesSubcategory/VehiclesSubcategory";
import SpeciesSubcategory from "../Subcategories/SpeciesSubcategory/SpeciesSubcategory";

const DetailsContent = (props) => {

    let content;

    switch (props.category) {
        case 'people':
            content = <div className={classes.DetailsContent}>
                <StarshipSubcategory starships={props.character.starships} label='Starships' />
                <VehiclesSubcategory vehicles={props.character.vehicles} label='Vehicles' />
                <FilmsSubcategory films={props.character.films} />
            </div>
            break;
        case 'films':
            content = <div className={classes.DetailsContent}>
                <OpeningCrawl crawl={props.film.opening_crawl} />
                <CharactersSubcategory characters={props.film.characters} label='Characters' />
                <SpeciesSubcategory species={props.film.species} label='Species' />
                <StarshipSubcategory starships={props.film.starships} label='Starships' />
                <VehiclesSubcategory vehicles={props.film.vehicles} label='Vehicles' />
            </div>;
            break;
        case 'starships':
            content = <div className={classes.DetailsContent}>
                <CharactersSubcategory characters={props.starship.pilots} label='Pilots' />
                <FilmsSubcategory films={props.starship.films} />
            </div>;
            break;
        case 'vehicles':
            content = <div className={classes.DetailsContent}>
                <CharactersSubcategory characters={props.vehicle.pilots} label='Pilots' />
                <FilmsSubcategory films={props.vehicle.films} />
            </div>;
            break;
        case 'species':
            content = <div className={classes.DetailsContent}>
                <CharactersSubcategory characters={props.specie.people} label='Characters' />
                <FilmsSubcategory films={props.specie.films} />
            </div>;
            break;
        case 'planets':
            content = <div className={classes.DetailsContent}>
                <CharactersSubcategory characters={props.planet.residents} label='Residents' />
                <FilmsSubcategory films={props.planet.films} />
            </div>;
            break;
        default:
            break;
    }

    return content
}

export default DetailsContent;