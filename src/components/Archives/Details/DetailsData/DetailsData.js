import classes from "./DetailsData.module.css";
import Loader from "../../../UI/Loader/Loader";

const DetailsData = (props) => {

    let content = <Loader />;

    switch (props.category) {
        case 'people':
            content = <div className={classes.DetailsData}>
                <div className={classes.Column}>
                    <p className={classes.DataField}><b>Gender:</b></p>
                    <p className={classes.DataField}>{props.data.gender}</p>
                    <p className={classes.DataField}><b>Height:</b></p>
                    <p className={classes.DataField}>{props.data.height} cm</p>
                    <p className={classes.DataField}><b>Mass:</b></p>
                    <p className={classes.DataField}>{props.data.mass} kg</p>
                    <p className={classes.DataField}><b>Birth Year:</b></p>
                    <p className={classes.DataField}>{props.data.birth_year}</p>
                    <p className={classes.DataField}><b>Skin Color:</b></p>
                    <p className={classes.DataField}>{props.data.skin_color}</p>
                    <p className={classes.DataField}><b>Hair Color:</b></p>
                    <p className={classes.DataField}>{props.data.hair_color}</p>
                    <p className={classes.DataField}><b>Eye Color:</b></p>
                    <p className={classes.DataField}>{props.data.eye_color}</p>
                </div>
            </div>;
            break;
        case 'films':
            if (props.data.producer !== undefined) {
                let splitProducers = props.data.producer.split(',');

                const producers = splitProducers.map(producer => {
                    return <p key={producer}>{producer.trimLeft()}</p>
                })

                const transformDate = (date) => {
                    const dateNumbers = date.split('-');
                    return `${dateNumbers[2]}.${dateNumbers[1]}.${dateNumbers[0]}`;
                }

                content = <div className={classes.DetailsData}>
                    <div className={classes.Column}>
                        <p className={classes.DataField}><b>Episode Number:</b></p>
                        <p className={classes.DataField}>{props.data.episode_id}</p>
                        <p className={classes.DataField}><b>Release Date:</b></p>
                        <p className={classes.DataField}>{transformDate(props.data.release_date)}</p>
                        <p className={classes.DataField}><b>Director:</b></p>
                        <p className={classes.DataField}>{props.data.director}</p>
                        <p className={classes.DataField}><b>Producer(s):</b></p>
                        <div className={classes.DataField}>
                            {producers}
                        </div>
                    </div>
                </div>;
            };
            break;
        case 'starships':
            content = <div className={classes.DetailsData}>
                <div className={classes.Column}>
                    <p className={classes.DataField}><b>Model:</b></p>
                    <p className={classes.DataField}>{props.data.model}</p>
                    <p className={classes.DataField}><b>Class:</b></p>
                    <p className={classes.DataField}>{props.data.starship_class}</p>
                    <p className={classes.DataField}><b>MGLT:</b></p>
                    <p className={classes.DataField}>{props.data.MGLT}</p>
                    <p className={classes.DataField}><b>Max Atmosphering Speed:</b></p>
                    <p className={classes.DataField}>{props.data.max_atmosphering_speed}</p>
                    <p className={classes.DataField}><b>Hyperdrive Rating:</b></p>
                    <p className={classes.DataField}>{props.data.hyperdrive_rating}</p>
                    <p className={classes.DataField}><b>Length:</b></p>
                    <p className={classes.DataField}>{props.data.length}m</p>
                    <p className={classes.DataField}><b>Crew Capacity:</b></p>
                    <p className={classes.DataField}>{props.data.crew}</p>
                    <p className={classes.DataField}><b>Passenger Capacity:</b></p>
                    <p className={classes.DataField}>{props.data.passengers}</p>
                    <p className={classes.DataField}><b>Cargo Capacity:</b></p>
                    <p className={classes.DataField}>{props.data.cargo_capacity}</p>
                    <p className={classes.DataField}><b>Cost In Credits:</b></p>
                    <p className={classes.DataField}>{props.data.cost_in_credits}</p>
                    <p className={classes.DataField}><b>Manufacturer:</b></p>
                    <p className={classes.DataField}>{props.data.manufacturer}</p>
                </div>
            </div>;
            break;
        case 'vehicles':
            content = <div className={classes.DetailsData}>
                <div className={classes.Column}>
                    <p className={classes.DataField}><b>Model:</b></p>
                    <p className={classes.DataField}>{props.data.model}</p>
                    <p className={classes.DataField}><b>Class:</b></p>
                    <p className={classes.DataField}>{props.data.vehicle_class}</p>
                    <p className={classes.DataField}><b>Max Atmosphering Speed:</b></p>
                    <p className={classes.DataField}>{props.data.max_atmosphering_speed}</p>
                    <p className={classes.DataField}><b>Length:</b></p>
                    <p className={classes.DataField}>{props.data.length}m</p>
                    <p className={classes.DataField}><b>Crew Capacity:</b></p>
                    <p className={classes.DataField}>{props.data.crew}</p>
                    <p className={classes.DataField}><b>Passenger Capacity:</b></p>
                    <p className={classes.DataField}>{props.data.passengers}</p>
                    <p className={classes.DataField}><b>Cargo Capacity:</b></p>
                    <p className={classes.DataField}>{props.data.cargo_capacity}</p>
                    <p className={classes.DataField}><b>Cost In Credits:</b></p>
                    <p className={classes.DataField}>{props.data.cost_in_credits}</p>
                    <p className={classes.DataField}><b>Manufacturer:</b></p>
                    <p className={classes.DataField}>{props.data.manufacturer}</p>
                </div>
            </div>;
            break;
        case 'species':
            content = <div className={classes.DetailsData}>
                <div className={classes.Column}>
                    <p className={classes.DataField}><b>Classification:</b></p>
                    <p className={classes.DataField}>{props.data.classification}</p>
                    <p className={classes.DataField}><b>Designation:</b></p>
                    <p className={classes.DataField}>{props.data.designation}</p>
                    <p className={classes.DataField}><b>Language:</b></p>
                    <p className={classes.DataField}>{props.data.language}</p>
                    <p className={classes.DataField}><b>Average Lifespan:</b></p>
                    <p className={classes.DataField}>{props.data.average_lifespan}</p>
                    <p className={classes.DataField}><b>Avarage Height:</b></p>
                    <p className={classes.DataField}>{props.data.average_height}cm</p>
                    <p className={classes.DataField}><b>Eye Color(s):</b></p>
                    <p className={classes.DataField}>{props.data.eye_colors}</p>
                    <p className={classes.DataField}><b>Hair Color(s):</b></p>
                    <p className={classes.DataField}>{props.data.hair_colors}</p>
                    <p className={classes.DataField}><b>Skin Color(s):</b></p>
                    <p className={classes.DataField}>{props.data.skin_colors}</p>
                </div>
            </div>;
            break;
        case 'planets':
            content = <div className={classes.DetailsData}>
                <div className={classes.Column}>
                    <p className={classes.DataField}><b>Population:</b></p>
                    <p className={classes.DataField}>{props.data.population}</p>
                    <p className={classes.DataField}><b>Diameter:</b></p>
                    <p className={classes.DataField}>{props.data.diameter}</p>
                    <p className={classes.DataField}><b>Orbital Period:</b></p>
                    <p className={classes.DataField}>{props.data.orbital_period}</p>
                    <p className={classes.DataField}><b>Rotation Period:</b></p>
                    <p className={classes.DataField}>{props.data.rotation_period}</p>
                    <p className={classes.DataField}><b>Terrain:</b></p>
                    <p className={classes.DataField}>{props.data.terrain}</p>
                    <p className={classes.DataField}><b>Climate:</b></p>
                    <p className={classes.DataField}>{props.data.climate}</p>
                    <p className={classes.DataField}><b>Gravity:</b></p>
                    <p className={classes.DataField}>{props.data.gravity}</p>
                    <p className={classes.DataField}><b>Surface Water:</b></p>
                    <p className={classes.DataField}>{props.data.surface_water}</p>
                </div>
            </div>;
            break;
        default:
            break;
    }

    return content
}

export default DetailsData;