import classes from "./DetailsHeader.module.css";

const DetailsHeader = (props) => {

    return (
        <div className={classes.DetailsHeader}>
            <h1>{props.title}</h1>
        </div>
    )
}

export default DetailsHeader;