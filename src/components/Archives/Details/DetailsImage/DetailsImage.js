import classes from "./DetailsImage.module.css";

const DetailsImage = (props) => {

    let image = require(`../../../../assets/images/${props.category}/${props.id}.png`).default;

    return (
        <div className={classes.DetailsImage}>
            <img className={classes.FlexImage} src={image} alt={props.alt}></img>
        </div>
    )
}

export default DetailsImage;