import classes from "./CharactersSubcategory.module.css";
import { useHistory } from "react-router";
import Loader from "../../../../UI/Loader/Loader";

const CharactersSubcategory = (props) => {

    const history = useHistory();

    let content = <div className={classes.CharactersSubcategory}>
        <h2 className={classes.Heading}>{props.label}</h2>
        <Loader/>
    </div>;

    if (props.characters !== undefined) {
        let characters = props.characters.map(characterUrl => {
            const id = characterUrl.split('/')[5];
            const path = require(`../../../../../assets/images/people/${id}.png`).default;
            return { id: id, path: path };
        })

        let characterImages = characters.map(character => {
            return <img className={classes.Image} src={character.path} alt={character.id} key={character.id} onClick={() => history.push(`/archives/people/details/${character.id}`)} />
        });

        if (characters.length === 0) {
            characterImages = <h3 className={classes.Center}>No Data</h3>
        }

        content = <div className={classes.CharactersSubcategory}>
            <h2 className={classes.Heading}>{props.label}</h2>
            <div className={classes.Images}>
                {characterImages}
            </div>
        </div>
    }

    return content;
}

export default CharactersSubcategory;