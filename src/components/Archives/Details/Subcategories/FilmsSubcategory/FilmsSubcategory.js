import classes from "./FilmsSubcategory.module.css";
import { useHistory } from "react-router";
import Loader from "../../../../UI/Loader/Loader";

const FilmsSubcategory = (props) => {

    const history = useHistory();

    let content = <div className={classes.CharacterFilms}>
        <h2 className={classes.Heading}>Films</h2>
        <Loader />
    </div>;

    if (props.films !== undefined) {
        let films = props.films.map(filmUrl => {
            const id = filmUrl.split('/')[5];
            const path = require(`../../../../../assets/images/films/${id}.png`).default;
            return { id: id, path: path };
        })

        let filmImages = films.map(films => {
            return <img className={classes.Image} src={films.path} alt={films.id} key={films.id} onClick={() => history.push(`/archives/films/details/${films.id}`)} />
        });

        if (films.length === 0) {
            filmImages = <h3 className={classes.Center}>No Data</h3>
        }

        content = <div className={classes.CharacterFilms}>
            <h2 className={classes.Heading}>Films</h2>
            <div className={classes.Images}>
                {filmImages}
            </div>
        </div>;
    }

    return content;
}

export default FilmsSubcategory;