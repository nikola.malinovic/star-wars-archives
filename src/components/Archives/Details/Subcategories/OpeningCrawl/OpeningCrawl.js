import classes from './OpeningCrawl.module.css';
import Loader from '../../../../UI/Loader/Loader';

const OpeningCrawl = (props) => {

    let content = <div className={classes.OpeningCrawl}>
        <h2 className={classes.Heading}>Opening Crawl</h2>
        <Loader/>
    </div>

    if (props.crawl !== undefined) {
        const text = props.crawl.split('\n').map((row, index) => {
            return <p key={index} className={classes.Text}>{row}</p>
        });

        content = <div className={classes.OpeningCrawl}>
            <h2 className={classes.Heading}>Opening Crawl</h2>
            {text}
        </div>;
    }

    return content;
}

export default OpeningCrawl;