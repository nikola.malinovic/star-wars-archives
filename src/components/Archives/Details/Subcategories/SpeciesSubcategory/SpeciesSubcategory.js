import classes from "./SpeciesSubcategory.module.css";
import { useHistory } from "react-router";
import Loader from "../../../../UI/Loader/Loader";

const SpeciesSubcategory = (props) => {

    const history = useHistory();

    let content = <div className={classes.SpeciesSubcategory}>
        <h2 className={classes.Heading}>{props.label}</h2>
        <Loader/>
    </div>;

    if (props.species !== undefined) {
        let species = props.species.map(speciesUrl => {
            const id = speciesUrl.split('/')[5];
            const path = require(`../../../../../assets/images/species/${id}.png`).default;
            return { id: id, path: path };
        })

        let speciesImages = species.map(specie => {
            return <img className={classes.Image}
                src={specie.path}
                alt={specie.id}
                key={specie.id}
                onClick={() => history.push(`/archives/species/details/${species.id}`)} />
        });

        if (species.length === 0) {
            speciesImages = <h3 className={classes.Center}>No Data</h3>
        }

        content = <div className={classes.SpeciesSubcategory}>
            <h2 className={classes.Heading}>{props.label}</h2>
            <div className={classes.Images}>
                {speciesImages}
            </div>
        </div>;
    }

    return content;
}

export default SpeciesSubcategory;