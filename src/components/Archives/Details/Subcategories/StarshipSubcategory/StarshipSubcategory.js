import classes from "./StarshipSubcategory.module.css";
import { useHistory } from "react-router";
import Loader from "../../../../UI/Loader/Loader";

const StarshipSubcategory = (props) => {

    const history = useHistory();

    let content = <div className={classes.StarshipSubcategory}>
        <h2 className={classes.Heading}>{props.label}</h2>
        <Loader/>
    </div>;

    if (props.starships !== undefined) {
        let starships = props.starships.map(starshipsUrl => {
            const id = starshipsUrl.split('/')[5];
            const path = require(`../../../../../assets/images/starships/${id}.png`).default;
            return { id: id, path: path };
        })

        let starshipImages = starships.map(starship => {
            return <img className={classes.Image}
                src={starship.path}
                alt={starship.id}
                key={starship.id}
                onClick={() => history.push(`/archives/starships/details/${starship.id}`)} />
        });

        if (starships.length === 0) {
            starshipImages = <h3 className={classes.Center}>No Data</h3>
        }

        content = <div className={classes.StarshipSubcategory}>
            <h2 className={classes.Heading}>{props.label}</h2>
            <div className={classes.Images}>
                {starshipImages}
            </div>
        </div>;
    }

    return content;
}

export default StarshipSubcategory;