import classes from "./VehiclesSubcategory.module.css";
import { useHistory } from "react-router";
import Loader from "../../../../UI/Loader/Loader";

const VehiclesSubcategory = (props) => {

    const history = useHistory();

    let content = <div className={classes.VehiclesSubcategory}>
        <h2 className={classes.Heading}>{props.label}</h2>
        <Loader/>
    </div>;

    if (props.vehicles !== undefined) {
        let vehicles = props.vehicles.map(vehiclesUrl => {
            const id = vehiclesUrl.split('/')[5];
            const path = require(`../../../../../assets/images/vehicles/${id}.png`).default;
            return { id: id, path: path };
        })

        let vehiclesImages = vehicles.map(vehicle => {
            return <img className={classes.Image}
                src={vehicle.path}
                alt={vehicle.id}
                key={vehicle.id}
                onClick={() => history.push(`/archives/vehicles/details/${vehicle.id}`)} />
        });

        if (vehicles.length === 0) {
            vehiclesImages = <h3 className={classes.Center}>No Data</h3>
        }

        content = <div className={classes.VehiclesSubcategory}>
            <h2 className={classes.Heading}>{props.label}</h2>
            <div className={classes.Images}>
                {vehiclesImages}
            </div>
        </div>;
    }

    return content;
}

export default VehiclesSubcategory;