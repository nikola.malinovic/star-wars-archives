import React from 'react';
import classes from './Categories.module.css';
import CategoryCard from './CategoryCard/CategoryCard';
import peopleImage from '../../assets/images/categories/people.jpeg';
import filmsImage from '../../assets/images/categories/films.jpeg';
import starshipsImage from '../../assets/images/categories/starships.png';
import vehiclesImage from '../../assets/images/categories/vehicles.png';
import speciesImage from '../../assets/images/categories/species.jpg';
import planetsImage from '../../assets/images/categories/planets.jpg';

const categories = (props) => {
    const categories = [
        { title: "people", image: peopleImage },
        { title: "films", image: filmsImage },
        { title: "starships", image: starshipsImage },
        { title: "vehicles", image: vehiclesImage },
        { title: "species", image: speciesImage },
        { title: "planets", image: planetsImage }
    ]

    return (
        <div className={classes.Categories}>
            {categories.map(category => (
                <CategoryCard title={category.title} image={category.image} key={category.title} />
            ))}
        </div>
    );
};

export default categories;