import React from 'react';
import classes from './CategoryCard.module.css';
import { Link } from 'react-router-dom';
import lightsaberSound from '../../../assets/sounds/lightsaber.mp3';

const categoryCard = (props) => {

    let snd = new Audio(lightsaberSound);
    snd.volume = 0.1;

    const onCategoryClick = () => {
        snd.currentTime = 0;
        snd.play();
    }

    return (
        <Link to={`/archives/${props.title}/?page=1`} className={classes.CategoryCard} onClick={() => onCategoryClick()}>
            <img src={props.image} className={classes.CardImage} alt={props.title + " category image"} />
            <h2 className={classes.CardTitle}>{props.title}</h2>
        </Link>
    );
};

export default categoryCard;