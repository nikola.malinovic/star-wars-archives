import React from 'react';
import classes from './About.module.css';

const About = (props) => (
    <div className={classes.About}>
        <p>
            As a passionate developer and a Star Wars fan, I was able to combine those two into one project. 
            While creating this app, I learned how to use ReactJS. 
            This is basicly my first app written in the mentioned JS framework.
        </p>
        <br />
        <p>Star Wars Archives is a project made for personal practice purposes. 
            I have no financial gains in it. All data rights are reserved to <b>SWAPI</b>. 
            You can find SWAPI's documentation <a href="https://swapi.dev/" target="_blank" rel="noreferrer">here</a>.
        </p>
    </div>
);

export default About;