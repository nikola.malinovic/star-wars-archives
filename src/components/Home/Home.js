import React from 'react';
import classes from './Home.module.css';
import About from './About/About';
import SkywalkersSection from './SkywalkersSection/SkywalkersSection';
import TieSection from './TieSection/TieSection';

const home = (props) => (
    <div className={classes.Home}>
        <About />
        <SkywalkersSection />
        <TieSection />
    </div>
);

export default home;