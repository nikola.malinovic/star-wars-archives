import React from 'react';
import classes from './SkywalkersSection.module.css';
import { useHistory } from 'react-router';

const SkywalkersSection = (props) => {

    const history = useHistory();

    let shmiImage = require(`../../../assets/images/people/43.png`).default;
    let anakinImage = require(`../../../assets/images/people/11.png`).default;
    let lukeImage = require(`../../../assets/images/people/1.png`).default;

    return (
        <div className={classes.SkywalkersSection}>
            <div className={classes.Images}>
                <img className={classes.Image} src={shmiImage} alt='Shmi Skywalker'></img>
                <img className={classes.Image} src={anakinImage} alt='Anakin Skywalker'></img>
                <img className={classes.Image} src={lukeImage} alt='Luke Skywalker'></img>
            </div>
            <div className={classes.Content}>
                <button className={classes.Button}
                    onClick={() => history.push(`/archives/people?search=skywalker&page=1`)}>
                    Explore Skywalkers
                </button>
            </div>
        </div>
    )
};

export default SkywalkersSection;