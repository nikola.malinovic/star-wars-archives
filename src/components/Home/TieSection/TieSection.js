import React from 'react';
import classes from './TieSection.module.css';
import { useHistory } from 'react-router';

const TieSection = (props) => {

    const history = useHistory();

    let tieStarfighterImage = require(`../../../assets/images/vehicles/8.png`).default;
    let tieInterceptorImage = require(`../../../assets/images/vehicles/26.png`).default;

    return (
        <div className={classes.TieSection}>
            <div className={classes.Content}>
                <button className={classes.Button}
                    onClick={() => history.push(`/archives/vehicles?search=tie&page=1`)}>
                    Explore TIEs
            </button>
            </div>
            <div className={classes.Images}>
                <img className={classes.Image} src={tieStarfighterImage} alt='TIE Starfighter'></img>
                <img className={classes.Image} src={tieInterceptorImage} alt='TIE Interceptor'></img>
            </div>
        </div>
    )
};

export default TieSection;