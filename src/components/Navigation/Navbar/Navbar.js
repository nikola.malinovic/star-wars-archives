import React from 'react';
import classes from './Navbar.module.css';
import logo from '../../../assets/images/sw-logo.png';
import { NavLink, Link, useLocation } from 'react-router-dom';
import lightsaberSound from '../../../assets/sounds/lightsaber.mp3';

const Navbar = () => {

    // used for component rerender after URL change
    const location = useLocation();

    let snd = new Audio(lightsaberSound);
    snd.volume = 0.1;

    const style = {
        width: "100%",
        height: "5px",
        background: "black",
        boxShadow: 'none',
        transition: '1s ease-in-out'
    }

    let link = location.pathname.split('/')[1];
    switch (link) {
        case '':
            style.background = 'rgba(206,255,190)';
            style.boxShadow = '0px 10px 13px -7px #000000, 0px 0px 5px 4px rgba(73,255,24,0.9)';
            break;
        case 'categories':
            style.background = 'rgb(175,208,255)';
            style.boxShadow = '0px 10px 13px -7px #000000, 0px 0px 5px 4px rgba(24,59,255,0.9)';
            break;
        case 'archives':
            style.background = 'rgb(235, 134, 134)';
            style.boxShadow = '0px 10px 13px -7px #000000, 0px 0px 5px 4px rgba(235,0,0,0.9)';
            break;
        default:
            break;
    }

    const onLinkClick = () => {
        snd.currentTime = 0;
        snd.play();
    }

    return (
        <div className={classes.Navbar}>
            <div className={classes.NavContent}>
                <Link to="/"><img src={logo} alt="swa-logo" width="100px" onClick={() => onLinkClick()} /></Link>
                <ul className={classes.NavigationItems}>
                    <li
                        id="green"
                        className={classes.NavigationItemGreen}
                        onClick={() => onLinkClick()}>
                        <NavLink to="/" exact>Home</NavLink>
                    </li>
                    <li
                        id="blue"
                        className={classes.NavigationItemBlue}
                        onClick={() => onLinkClick()}>
                        <NavLink to="/categories">Categories</NavLink>
                    </li>
                    <li
                        id="red"
                        className={classes.NavigationItemRed}
                        onClick={() => onLinkClick()}>
                        <NavLink to="/archives">Archives</NavLink>
                    </li>
                </ul>
            </div>
            <div style={style}></div>
        </div>
    );
};

export default Navbar;