import React from 'react';
import classes from './PageContent.module.css';
import { Route, Switch } from 'react-router';
import Home from '../Home/Home';
import Categories from '../Categories/Categories';
import Archives from '../Archives/Archives';

const pageContent = (props) => (
    <div className={classes.PageContent}>
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/categories" component={Categories} />
            <Route path="/archives" component={Archives} />
        </Switch>
    </div>
);

export default pageContent;