import classes from './Pagination.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

const Pagination = (props) => {

    let activePage = props.initialPage;
    let pages = [];
    let pageElements = [];

    for (var i = 1; i <= props.size; i++) {
        pages.push(i);
    };
    pageElements = pages.map(page => {
        return (
            <div
                key={page}
                className={page === activePage ? classes.ActivePageElement : classes.PageElement}
                onClick={() => props.changePage(page)}>
                {page}
            </div>
        )
    });

    return (
        <div className={classes.Pagination}>
            <div className={classes.PageElement} onClick={() => props.changePage(activePage - 1)}>
                <div><FontAwesomeIcon icon={faChevronLeft} /></div>
            </div>
            {pageElements}
            <div className={classes.PageElement} onClick={() => props.changePage(activePage + 1)}>
                <div><FontAwesomeIcon icon={faChevronRight} /></div>
            </div>
        </div>
    )
}

export default Pagination;