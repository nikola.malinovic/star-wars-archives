import classes from './SearchBar.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { useState } from 'react';
import { useHistory } from "react-router-dom";

const SearchBar = () => {

    const [search, setSearch] = useState('');
    const history = useHistory();

    const onSearch = () => {
        const currentUrl = history.location.pathname;
        history.push(`${currentUrl}?search=${search}&page=1`);
    }

    const onEnterSearch = (e) => {
        // char code === 13 is 'Enter' key on keyboard
        if (e.charCode === 13) {
            e.preventDefault();
            onSearch();
        }
    }

    return (
        <div className={classes.SearchBar} >
            <input
                className={classes.SearchInput}
                type="text"
                placeholder="Search"
                onChange={(e) => setSearch(e.target.value)}
                onKeyPress={(e) => onEnterSearch(e)} />
            <button
                className={classes.SearchButton}
                href="#"
                onClick={() => onSearch()}>
                <FontAwesomeIcon icon={faSearch} size="lg" />
            </button>
        </div>
    );
}

export default SearchBar;