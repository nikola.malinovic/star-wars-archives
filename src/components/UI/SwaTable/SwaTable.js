import classes from './SwaTable.module.css';
import { useHistory } from 'react-router';

const SwaTable = (props) => {

    const history = useHistory();

    let headerData = [];
    let entryData = [];

    switch (props.category) {
        case 'people': {
            headerData = ['Name', 'Gender', 'Birth', 'Height', 'Mass'];
            entryData = props.displayData.map(dataRow => (
                <tr className={classes.SwaTableRow}
                    key={dataRow.url}
                    onClick={() => history.push(`/archives/${props.category}/details/${dataRow.id}`)}>
                    <td>{dataRow.name}</td>
                    <td>{dataRow.gender}</td>
                    <td>{dataRow.birth_year}</td>
                    <td>{dataRow.height}</td>
                    <td>{dataRow.mass}</td>
                </tr>
            ));
            break;
        }
        case 'films': {
            headerData = ['Title', 'Episode', 'Director', 'Producer', 'Release'];
            entryData = props.displayData.map(dataRow => (
                <tr className={classes.SwaTableRow}
                    key={dataRow.url}
                    onClick={() => history.push(`/archives/${props.category}/details/${dataRow.id}`)}>
                    <td>{dataRow.title}</td>
                    <td>{dataRow.episode_id}</td>
                    <td>{dataRow.director}</td>
                    <td>{dataRow.producer}</td>
                    <td>{dataRow.release_date}</td>
                </tr>
            ));
            break;
        }
        case 'starships': {
            headerData = ['Name', 'Model', 'Class', 'Crew', 'Max Speed'];
            entryData = props.displayData.map(dataRow => (
                <tr className={classes.SwaTableRow}
                    key={dataRow.url}
                    onClick={() => history.push(`/archives/${props.category}/details/${dataRow.id}`)}>
                    <td>{dataRow.name}</td>
                    <td>{dataRow.model}</td>
                    <td>{dataRow.starship_class}</td>
                    <td>{dataRow.crew}</td>
                    <td>{dataRow.max_atmosphering_speed}</td>
                </tr>
            ));
            break;
        }
        case 'vehicles': {
            headerData = ['Name', 'Model', 'Cost', 'Crew', 'Max Speed'];
            entryData = props.displayData.map(dataRow => (
                <tr className={classes.SwaTableRow}
                key={dataRow.url}
                onClick={() => history.push(`/archives/${props.category}/details/${dataRow.id}`)}>
                    <td>{dataRow.name}</td>
                    <td>{dataRow.model}</td>
                    <td>{dataRow.cost_in_credits}</td>
                    <td>{dataRow.crew}</td>
                    <td>{dataRow.max_atmosphering_speed}</td>
                </tr>
            ));
            break;
        }
        case 'species': {
            headerData = ['Name', 'Classification', 'Language', 'Average Lifespan', 'Average Height'];
            entryData = props.displayData.map(dataRow => (
                <tr className={classes.SwaTableRow}
                    key={dataRow.url}
                    onClick={() => history.push(`/archives/${props.category}/details/${dataRow.id}`)}>
                    <td>{dataRow.name}</td>
                    <td>{dataRow.classification}</td>
                    <td>{dataRow.language}</td>
                    <td>{dataRow.average_lifespan}</td>
                    <td>{dataRow.average_height}</td>
                </tr>
            ));
            break;
        }
        case 'planets': {
            headerData = ['Name', 'Population', 'Terrain', 'Diameter', 'Climate'];
            entryData = props.displayData.map(dataRow => (
                <tr className={classes.SwaTableRow}
                    key={dataRow.url}
                    onClick={() => history.push(`/archives/${props.category}/details/${dataRow.id}`)}>
                    <td>{dataRow.name}</td>
                    <td>{dataRow.population}</td>
                    <td>{dataRow.terrain}</td>
                    <td>{dataRow.diameter}</td>
                    <td>{dataRow.climate}</td>
                </tr>
            ));
            break;
        }
        default: {
            headerData = ['Name', 'Gender', 'Birth', 'Height', 'Mass'];
            entryData = props.displayData.map(dataRow => (
                <tr className={classes.SwaTableRow} key={dataRow.url}>
                    <td>{dataRow.name}</td>
                    <td>{dataRow.gender}</td>
                    <td>{dataRow.birth_year}</td>
                    <td>{dataRow.height}</td>
                    <td>{dataRow.mass}</td>
                </tr>
            ));
            break;
        }
    }

    return (
        <div className={classes.Ass}>
            <table className={classes.SwaTable} cellSpacing="0" cellPadding="0">
                <thead>
                    <tr className={classes.SwaTableHeaderRow}>
                        {headerData.map(data => {
                            return <th className={classes.SwaHeaderColumn} key={data}>{data}</th>
                        })}
                    </tr>
                </thead>
                <tbody>
                    {entryData}
                </tbody>
            </table>
        </div>
    );
}

export default SwaTable;